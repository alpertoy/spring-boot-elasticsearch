package io.gitlab.alpertoy.exception;

public class IllegalApiParamException extends RuntimeException {

	private static final long serialVersionUID = -3610913777411764911L;

	public IllegalApiParamException(String s) {
		super(s);
	}

}
