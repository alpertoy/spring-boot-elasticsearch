package io.gitlab.alpertoy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprinbootElasticsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprinbootElasticsearchApplication.class, args);
	}

}
