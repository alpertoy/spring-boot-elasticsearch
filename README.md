## A Car API with Spring Data Elasticsearch

Make sure elasticsearch and kibana is running

### Kibana UI

`localhost:5601`

### Swagger UI

`localhost:8001/swagger-ui.html`

### Postman Collection
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b9502bec8f30cff6cada)
